# Pizza 365 Admin v1.0
## 📄 Description 
- ### **Authority for Admin:** *checking order list, creating order, checking order detail*
## ✨ Feature
+ ***Order List***
![Order List](images/order-list.PNG)
+ Order Detail
![Order Detail](images/order-detail.PNG)
+ Create Order
![Create Order](images/create-order.PNG)
+ Sample Api - *checking server api*
![Check Api](images/sample-api.PNG)
## 🧱 Technology
+ Front-end:
  : 1.[Bootstrap 4](https://getbootstrap.com/docs/4.6/getting-started/introduction/)
  : 2.Javascript
  : 3.Jquery 3
  : 4.Ajax
  : 5.Datatable 
  : 6.JSON
  : 7.Bootstrap 4
