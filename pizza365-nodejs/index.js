const express = require("express"); // import thư viện express
const mongoose = require("mongoose") // import thư viện mongoose
const path = require("path"); // import path
const cors = require('cors'); // import cors
const app = express(); // khởi tạo app express
const port = 8000; // khai báo cổng chạy api
const drinkRouter = require("./app/routes/drink.router"); // import drink router
const voucherRouter = require("./app/routes/voucher.router"); // import voucher router
const userRouter = require("./app/routes/user.router"); // import user router
const orderRouter = require("./app/routes/order.router"); // import order router

// khai báo kết nối mongoDB qua mongoose
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365")
.then(() => console.log("Successfully connected !"))
.catch((err) => console.log(err.message));

// cấu hình để sử dụng json
app.use(express.json());

// định nghĩa middleware để phục vụ các tệp tĩnh (css, hình ảnh, ...)
app.use(express.static(__dirname + "/views"));

// middleware gắn Access-Control-Allow-Origin vào response
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// định nghĩa route cho trang chủ
app.get("/", (req, res) => {
  // đường dẫn đến file html trong thư mục views 
  console.log(__dirname);
  const indexPath = path.join(__dirname, "./views/pizza365.v1.0.html");
  res.sendFile(indexPath);
});

// cho phép cors với route
app.use(cors());

// Router Middleware
// sử dụng drink router middleware
app.use("/drinks", drinkRouter);

// sử dụng voucher router middleware
app.use("/vouchers", voucherRouter);

// sử dụng user router middleware
app.use("/users", userRouter);

// sử dụng order router middleware
app.use("/orders", orderRouter);

// listen ở cổng 8000
app.listen(port, () => console.log("Server running on port " + port + "."));