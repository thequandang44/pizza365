const express = require("express"); // import thư viện express
const router = express.Router(); // khởi tạo router

// Middleware để log thông tin API cho mỗi route CRUD
const logApiMiddleware = (req, res, next) => {
  console.log(`[${new Date().toLocaleString()}] ${req.method} ${req.originalUrl}`);
  next();
}

// imporrt controllers
const {
  getAllDrinks,
  createDrink,
  getDrinkById,
  updateDrinkById,
  deleteDrinkById
} = require("../controllers/drink.controller");

// sử dụng application middleware cho tất cả các route
router.use(logApiMiddleware);

// khai báo các api router
router.get("/", getAllDrinks);
router.post("/", createDrink);
router.get("/:drinkid", getDrinkById);
router.put("/:drinkid", updateDrinkById);
router.delete("/:drinkid", deleteDrinkById);

// xuất router
module.exports = router;
