const express = require("express"); // import thư viện express
const router = express.Router(); // khởi tạo router

// Middleware để log thông tin API cho mỗi route CRUD
const logApiMiddleware = (req, res, next) => {
  console.log(`[${new Date().toLocaleString()}] ${req.method} ${req.originalUrl}`);
  next();
}

// imporrt controllers
const {
  getAllVouchers,
  createVoucher,
  getVoucherById,
  updateVoucherById,
  deleteVoucherById
} = require("../controllers/voucher.controller");

// sử dụng application middleware cho tất cả các route
router.use(logApiMiddleware);

// khai báo các api router
router.get("/", getAllVouchers);
router.post("/", createVoucher);
router.get("/:voucherid", getVoucherById);
router.put("/:voucherid", updateVoucherById);
router.delete("/:voucherid", deleteVoucherById);

// xuất router
module.exports = router;
