const express = require("express"); // import thư viện express
const router = express.Router(); // khởi tạo router

const orderController = require("../controllers/order.controller"); // import order controller

// Middleware để log thông tin API cho mỗi route CRUD
const logApiMiddleware = (req, res, next) => {
  console.log(`[${new Date().toLocaleString()}] ${req.method} ${req.originalUrl}`);
  next();
}
// sử dụng application middleware cho tất cả các route
router.use(logApiMiddleware);

// khai báo các api router
router.get("/", orderController.getAllOrders);
router.post("/", orderController.createOrder);
router.get("/:orderid", orderController.getOrderById);
router.put("/:orderid", orderController.updateOrderById);
router.delete("/:orderid", orderController.deleteorderById);

// xuất router
module.exports = router;