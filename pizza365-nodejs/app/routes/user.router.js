const express = require("express"); // import thư viện express
const router = express.Router(); // khởi tạo router

const userController = require("../controllers/user.controller"); // import user controller

// Middleware để log thông tin API cho mỗi route CRUD
const logApiMiddleware = (req, res, next) => {
  console.log(`[${new Date().toLocaleString()}] ${req.method} ${req.originalUrl}`);
  next();
}
// sử dụng application middleware cho tất cả các route
router.use(logApiMiddleware);

// khai báo các api router
router.get("/", userController.getAllUser);
// phải đặt các hàm get (limit, skip, sort) trước các hàm id trong router
router.get("/limit-users", userController.getAllLimitUser);
router.get("/skip-users", userController.getAllSkipUser);
router.get("/sort-users", userController.getAllSortUser);
router.post("/", userController.createUser);
router.get("/:userid", userController.getUserById);
router.put("/:userid", userController.updateUserById);
router.delete("/:userid", userController.deleteUserById);

// xuất router
module.exports = router;
