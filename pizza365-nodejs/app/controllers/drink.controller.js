const drinkModel = require("../models/drink.model"); // import drink model
const mongoose = require("mongoose");

// controller api get all drinks
const getAllDrinks = async (req, res) => {
  // b1: thu thập dữ liệu (ko có)
  // b2: validate dữ liệu (ko có)
  // b3: xử lí dữ liệu
  try {
    const result = await drinkModel.find();
    return res.status(200).json({
      message: "Lấy danh sánh drink thành công",
      data: result
    });
  } catch (error) {
    // dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

// controller api create drink
const createDrink = async (req, res) => {
  // b1: thu thập dữ liệu
  const {
    reqMaNuocUong,
    reqTenNuocUong,
    reqDonGia
  } = req.body;

  // b2: validate dữ liệu
  if (!reqMaNuocUong) {
    return res.status(400).json({
      message: "Xin nhập mã nước uống"
    })
  }
  if (!reqTenNuocUong) {
    return res.status(400).json({
      message: "Xin nhập tên nước uống"
    })
  }
  if (!reqDonGia) {
    return res.status(400).json({
      message: "Xin nhập đon giá"
    })
  }

  // b3: xử lí dữ liệu
  try {
    const newDrink = {
      maNuocUong: reqMaNuocUong,
      tenNuocUong: reqTenNuocUong,
      donGia: reqDonGia
    };
    const result = await drinkModel.create(newDrink);
    return res.status(201).json({
      message: "Thêm drink mới thành công",
      data: result
    })
  } catch (error) {
    // dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
};

// controller api get drink by id
const getDrinkById = async (req, res) => {
  // b1: thu thập dữ liệu
  const drinkid = req.params.drinkid;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(drinkid)) {
    return res.status(400).json({
      message: "Drink ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await drinkModel.findById(drinkid);
    if (result) {
      return res.status(200).json({
        message: "Lấy drink theo id thành công",
        data: result
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy drink theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

// controller api update drink by id
const updateDrinkById = async (req, res) => {
  // b1: thu thập dữ liệu
  const drinkid = req.params.drinkid;
  const {
    reqMaNuocUong,
    reqTenNuocUong,
    reqDonGia
  } = req.body;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(drinkid)) {
    return res.status(400).json({
      message: "Drink ID không hợp lệ"
    });
  }
  if (reqMaNuocUong === "") {
    return res.status(400).json({
      message: "Mã nước uống không hợp lệ"
    });
  }
  if (reqTenNuocUong === "") {
    return res.status(400).json({
      message: "Tên nước uống không hợp lệ"
    });
  }
  if (reqDonGia === "") {
    return res.status(400).json({
      message: "Đơn giá không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const updatedDrink = {};
    if (reqMaNuocUong) {
      updatedDrink.maNuocUong = reqMaNuocUong;
    }
    if (reqTenNuocUong) {
      updatedDrink.tenNuocUong = reqTenNuocUong;
    }
    if (reqDonGia) {
      updatedDrink.donGia = reqDonGia;
    }
    const result = await drinkModel.findByIdAndUpdate(drinkid, updatedDrink);
    if (result) {
      return res.status(200).json({
        message: "Cập nhật drink theo id thành công",
        data: result
      })
    } else {
      return res.status(404).json({
        message: "Không tìm thấy drink tương ứng id"
      })
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

// controller api delete drink by id
const deleteDrinkById = async (req, res) => {
  // b1: thu thập dữ liệu
  const drinkid = req.params.drinkid;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(drinkid)) {
    return res.status(400).json({
      message: "Drink ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await drinkModel.findByIdAndDelete(drinkid);

    if (result) {
      return res.status(200).json({
        message: "Xóa drink theo id thành công"
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy course theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

// export all controllers
module.exports = {
  getAllDrinks,
  createDrink,
  getDrinkById,
  updateDrinkById,
  deleteDrinkById
};