const voucherModel = require("../models/voucher.model"); // import voucher model
const mongoose = require("mongoose");

// controller api get all vouchers
const getAllVouchers = async (req, res) => {
  // b1: thu thập dữ liệu (ko có)
  // b2: validate dữ liệu (ko có)
  // b3: xử lí dữ liệu
  try {
    const result = await voucherModel.find();
    return res.status(200).json({
      message: "Lấy danh sánh voucher thành công",
      data: result
    });
  } catch (error) {
    // dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

// controller api create all voucher
const createVoucher = async (req, res) => {
  // b1: thu thập dữ liệu
  const {
    reqMaVoucher,
    reqPhanTramGiamGia,
    reqGhiChu
  } = req.body;
  
  // b2: validate dữ liệu
  if (!reqMaVoucher) {
    return res.status(400).json({
      message: "Xin nhập mã voucher"
    });
  }
  if (!reqPhanTramGiamGia) {
    return res.status(400).json({
      message: "Xin nhập phần trăm giảm giá"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const newVoucher = {
      maVoucher: reqMaVoucher,
      phanTramGiamGia: reqPhanTramGiamGia,
      ghiChu: reqGhiChu
    };

    const result = await voucherModel.create(newVoucher);

    return res.status(201).json({
      message: "Tạo voucher thành công",
      data: result
    });
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

// controller api get voucher by id
const getVoucherById = async (req, res) => {
  // b1: thu thập dữ liệu
  const voucherid = req.params.voucherid;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(voucherid)) {
    return res.status(400).json({
      message: "voucher ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await voucherModel.findById(voucherid);

    if (result) {
      return res.status(200).json({
        message: "Lấy voucher theo id thành công",
        data: result
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy voucher theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

// controller api update voucher by id
const updateVoucherById = async (req, res) => {
  // b1: thu thập dữ liệu
  const voucherid = req.params.voucherid;
  const {
    reqMaVoucher,
    reqPhanTramGiamGia,
    reqGhiChu
  } = req.body;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(voucherid)) {
    return res.status(400).json({
      message: "voucher ID không hợp lệ"
    });
  }
  if (reqMaVoucher === "") {
    return res.status(400).json({
      message: "Mã voucher không hợp lệ"
    });
  }
  if (reqPhanTramGiamGia === "") {
    return res.status(400).json({
      message: "Phần trăm giảm giá không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const updatedVoucher = {};
    if (reqMaVoucher) {
      updatedVoucher.maVoucher = reqMaVoucher;
    }
    if (reqPhanTramGiamGia) {
      updatedVoucher.phanTramGiamGia = reqPhanTramGiamGia;
    }
    if (reqGhiChu) {
      updatedVoucher.ghiChu = reqGhiChu;
    }
    const result = await voucherModel.findByIdAndUpdate(voucherid, updatedVoucher);
    if (result) {
      return res.status(200).json({
        message: "Cập nhật voucher theo id thành công",
        data: result
      })
    } else {
      return res.status(404).json({
        message: "Không tìm thấy voucher tương ứng id"
      })
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

// controller api delete voucher by id
const deleteVoucherById = async (req, res) => {
  // b1: thu thập dữ liệu
  const voucherid = req.params.voucherid;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(voucherid)) {
    return res.status(400).json({
      message: "voucher ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await voucherModel.findByIdAndDelete(voucherid);

    if (result) {
      return res.status(200).json({
        message: "Xóa voucher theo id thành công"
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy voucher theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

// export all controllers
module.exports = {
  getAllVouchers,
  createVoucher,
  getVoucherById,
  updateVoucherById,
  deleteVoucherById
};