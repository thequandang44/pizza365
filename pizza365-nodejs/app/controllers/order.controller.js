const mongoose = require("mongoose"); // import thư viện mongoose
const orderModel = require("../models/order.model"); // import model order
const userModel = require("../models/user.model"); // import model user

const getAllOrders = async (req, res) => {
  // b1: thu thập dữ liệu (ko có)
  const userid = req.query.userid;
  // b2: validate dữ liệu (ko có)
  if (userid !== undefined && !mongoose.Types.ObjectId.isValid(userid)) {
    return res.status(400).json({
      message: "User ID không hợp lệ"
    })
  }
  // b3: xử lí dũ liệu
  try {
    const populatedUser = await userModel.findById(userid).populate("orders");
    if (userid === undefined) {
      const orderList = await orderModel.find();
      if (orderList && orderList.length > 0) {
        return res.status(200).json({
          message: "Lấy danh sách order thành công",
          data: orderList
        })
      } else {
        return res.status(404).json({
          message: "Không tìm thấy order nào"
        })
      }
    } 
    else if (populatedUser) {
      return res.status(200).json({
        message: "Lấy tất cả order của user thành công",
        data: populatedUser.orders
      })
    } else {
      return res.status(404).json({
        message: "Không tìm thấy user phù hợp",
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
};

const createOrder = async (req, res) => {
  // b1: thu thập dữ liệu (ko có)
  const order = req.body;

  // b2: validate dữ liệu (ko có)
  if (!mongoose.Types.ObjectId.isValid(order.userid)) {
    return res.status(400).json({
      message: "User ID không hợp lệ"
    })
  }
  if (!order.pizzaSize) {
    return res.status(400).json({
      message: "Xin mời nhập kích cỡ"
    });
  }
  if (!order.pizzaType) {
    return res.status(400).json({
      message: "Xin mời nhập loại bánh"
    });
  }
  if (!order.status) {
    return res.status(400).json({
      message: "Xin mời nhập trạng thái"
    });
  }

  // b3: xử lí dũ liệu
  const newOrder = {
    orderCode: order.orderCode,
    pizzaSize: order.pizzaSize,
    pizzaType: order.pizzaType,
    status: order.status
  }
  try {
    const createdOrder = await orderModel.create(newOrder);
    const updatedUser = await userModel.findByIdAndUpdate(order.userid, {
      $push: { orders: createdOrder._id }
    });
    return res.status(201).json({
      message:"Tạo order và đẩy vào user thành công",
      data: createdOrder,
      user: updatedUser
    })
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
};

const getOrderById = async (req, res) => {
  // b1: thu thập dữ liệu (ko có)
  const orderid = req.params.orderid;

  // b2: validate dữ liệu (ko có)
  if (!mongoose.Types.ObjectId.isValid(orderid)) {
    return res.status(400).json({
      message: "Order ID không hợp lệ"
    })
  }

  // b3: xử lí dũ liệu
  try {
    const foundOrder = await orderModel.findById(orderid);
    if (foundOrder) {
      return res.status(200).json({
        message: "Lấy order theo id thành công",
        data: foundOrder
      })
    } else {
      return res.status(404).json({
        message: "Không tìm thấy order tương ứng id"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
};

const updateOrderById = async (req, res) => {
  // b1: thu thập dữ liệu (ko có)
  const orderid = req.params.orderid;
  const order = req.body;

  // b2: validate dữ liệu (ko có)
  if (!mongoose.Types.ObjectId.isValid(orderid)) {
    return res.status(400).json({
      message: "Order ID không hợp lệ"
    })
  }

  // b3: xử lí dũ liệu
  try {
    const updatedInfo = {
      orderCode: order.orderCode,
      pizzaSize: order.pizzaSize,
      pizzaType: order.pizzaType,
      status: order.status
    };
    const updatedOrder = await orderModel.findByIdAndUpdate(orderid, updatedInfo);
    if (updatedOrder) {
      return res.status(200).json({
        message: "Cập nhật order theo id thành công",
        data: updatedOrder
      })
    } else {
      return res.status(404).json({
        message: "Không tìm thấy order tương ứng id"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
};

const deleteorderById = async (req, res) => {
  // b1: thu thập dữ liệu (ko có)
  const orderid = req.params.orderid;
  const userid = req.query.userid;

  // b2: validate dữ liệu (ko có)
  if (!mongoose.Types.ObjectId.isValid(orderid)) {
    return res.status(400).json({
      message: "Order ID không hợp lệ"
    })
  }

  // b3: xử lí dũ liệu
  try {
    if (userid !== undefined) {
      await userModel.findByIdAndUpdate(userid, {
        $pull: { orders: orderid }
      })
    }

    const deletedOrder = await orderModel.findByIdAndDelete(orderid);
    if (deletedOrder) {
      return res.status(200).json({
        message: "Xóa order theo id thành công"
      })
    } else {
      return res.status(404).json({
        message: "Không tìm thấy order tương ứng id"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
};

module.exports = {
  getAllOrders,
  createOrder,
  getOrderById,
  updateOrderById,
  deleteorderById
};