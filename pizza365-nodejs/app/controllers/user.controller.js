const userModel = require("../models/user.model") // import model user
const mongoose = require("mongoose"); // import mongoose

const getAllUser = async (req, res) => {
  // b1: thu thập dữ liệu (ko có)
  // b2: validate dữ liệu (ko có)
  // b3: xử lí dũ liệu
  try {
    const result = await userModel.find();

    return res.status(200).json({
      message: "Lấy danh sách user thành công",
      data: result 
    });
  } catch (error) {
    // dùng hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const getAllLimitUser = async (req, res) => {
  // b1: thu thập dữ liệu 
  let limit = req.query.limit;
  // b2: validate dữ liệu (ko có)
  // b3: xử lí dũ liệu
  try {
    const result = await userModel.find().limit(limit);

    return res.status(200).json({
      message: "Lấy danh sách limit user thành công",
      limit,
      data: result 
    });
  } catch (error) {
    // dùng hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const getAllSkipUser = async (req, res) => {
  // b1: thu thập dữ liệu 
  let skip = req.query.skip;
  // b2: validate dữ liệu (ko có)
  // b3: xử lí dũ liệu
  try {
    const result = await userModel.find().skip(skip);

    return res.status(200).json({
      message: "Lấy danh sách skip user thành công",
      skip,
      data: result 
    });
  } catch (error) {
    // dùng hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const getAllSortUser = async (req, res) => {
  // b1: thu thập dữ liệu 
  // b2: validate dữ liệu (ko có)
  // b3: xử lí dũ liệu
  try {
    const result = await userModel.find().sort({ fullName: "asc" });
    console.log("hello")
    return res.status(200).json({
      message: "Lấy danh sách alphabet sort user thành công",
      data: result 
    });
  } catch (error) {
    // dùng hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const createUser = async (req, res) => {
  // b1: thu thập dữ liệu
  const user = req.body;
  
  // b2: validate dữ liệu
  if (!user.fullName) {
    return res.status(400).json({
      message: "Xin mời nhập tên"
    });
  }
  if (!user.email) {
    return res.status(400).json({
      message: "Xin mời nhập email"
    });
  }
  if (!user.address) {
    return res.status(400).json({
      message: "Xin mời nhập địa chỉ"
    });
  }
  if (!user.phone) {
    return res.status(400).json({
      message: "Xin mời nhập số điện thoại"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const newUser = {
      fullName: user.fullName,
      email: user.email,
      address: user.address,
      phone: user.phone,
    };

    const result = await userModel.create(newUser);

    return res.status(201).json({
      message: "Tạo user thành công",
      data: result
    });
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const getUserById = async (req, res) => {
  // b1: thu thập dữ liệu
  const userid = req.params.userid;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userid)) {
    return res.status(400).json({
      message: "User ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await userModel.findById(userid);

    if (result) {
      return res.status(200).json({
        message: "Lấy user theo id thành công",
        data: result
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy user theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const updateUserById = async (req, res) => {
  // b1: thu thập dữ liệu
  const userid = req.params.userid;
  const user = req.body;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userid)) {
    return res.status(400).json({
      message: "User ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const newUserUpdated = {};
    if (user.fullName) {
      newUserUpdated.fullName = user.fullName;
    }
    if (user.email) {
      newUserUpdated.email = user.email;
    }
    if (user.address) {
      newUserUpdated.address = user.address;
    }
    if (user.phone) {
      newUserUpdated.phone = user.phone;
    }

    const result = await userModel.findByIdAndUpdate(userid, newUserUpdated);

    if (result) {
      return res.status(200).json({
        message: "Cập nhật user theo id thành công",
        data: result
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy user theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const deleteUserById = async (req, res) => {
  // b1: thu thập dữ liệu
  const userid = req.params.userid;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userid)) {
    return res.status(400).json({
      message: "User ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await userModel.findByIdAndDelete(userid);

    if (result) {
      return res.status(200).json({
        message: "Xóa user theo id thành công"
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy user theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

module.exports = {
  getAllUser,
  getAllLimitUser,
  getAllSkipUser,
  getAllSortUser,
  createUser,
  getUserById,
  updateUserById,
  deleteUserById
};