const mongoose = require("mongoose"); // import mongoose
const Schema = mongoose.Schema; // khai báo schema lấy ra từ mongoose
// khởi tạo user schema
const userSchema = new Schema({
  fullName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  address: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true,
    unique: true
  },
  orders: [
    {
      type: mongoose.Types.ObjectId,
      ref: "Order"
    }
  ]
}, {
  timestapms: true
});

// export model
module.exports = mongoose.model("User", userSchema);