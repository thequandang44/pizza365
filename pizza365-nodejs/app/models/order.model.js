const mongoose = require("mongoose"); // import mongoose
const Schema = mongoose.Schema; // khai báo schema lấy ra từ mongoose
// khởi tạo order schema
const orderSchema = new Schema({
  orderCode: {
    type: String,
    unique: true,
    default: function() {
      let randomNumber = Math.floor(Math.random() * Math.pow(10, 10));
      let newString = randomNumber.toString(36);
      let result = newString.slice(0, newString.length - 3) + 
        newString.slice(newString.length - 3, newString.length).toUpperCase();
      return result;
    }
  },
  pizzaSize: {
    type: String,
    required: true
  },
  pizzaType: {
    type: String,
    required: true
  },
  voucher: {
    type: mongoose.Types.ObjectId,
    ref: "Voucher"
  },
  drink: {
    type: mongoose.Types.ObjectId,
    ref: "Drink"
  },
  status: {
    type: String,
    required: true
  }
}, {
  timestapms: true
});

// export model
module.exports = mongoose.model("Order", orderSchema);