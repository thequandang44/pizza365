const mongoose = require("mongoose"); // import mongoose
const Schema = mongoose.Schema; // khai báo schema lấy ra từ mongoose
// khởi tạo voucher schema
const voucherSchema = new Schema({
  maVoucher: {
    type: String,
    required: true,
    unique: true
  },
  phanTramGiamGia: {
    type: Number,
    required: true
  },
  ghiChu: {
    type: String,
    required: false
  }
}, {
  timestapms: true
});

// export model
module.exports = mongoose.model("Voucher", voucherSchema);