const mongoose = require("mongoose"); // import mongoose
const Schema = mongoose.Schema; // khai báo schema lấy ra từ mongoose
// khởi tạo drink schema
const drinkSchema = new Schema({
  maNuocUong: {
    type: String,
    required: true,
    unique: true
  },
  tenNuocUong: {
    type: String,
    required: true
  },
  donGia: {
    type: Number,
    required: true
  }
}, {
  timestapms: true
});

// export model
module.exports = mongoose.model("Drink", drinkSchema);