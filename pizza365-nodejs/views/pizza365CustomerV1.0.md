# Pizza 365 v0.9 for Customer 
## 📄 Description 
- ### **Order's steps for customer:** *select (combo, pizza and drink), enter information, check order and send order*
## ✨ Feature
+ ***Homepage of Pizza 365***
![homepage](images/homepage-pizza-365.PNG)
+ Size Combos 
![sizecombo](images/size-combo.PNG)
+ Pizza Types
![pizzatype](images/pizza-type.PNG)
+ Drinks
![drink](images/drink.PNG)
+ Form Contact
![form contact](images/form-contact.PNG)
+ Form Check Order
![form check order](images/check-order.PNG)
+ Form Send Order
![form send order](images/send-order.PNG)
## 🧱 Technology
+ Front-end: 
  : 1. Bootstrap 4
  : 2. Javascript 
  : 3. Ajax
  : 4. Local storage
  : 5. JSON 
  : 6. Google fonts
  : 7. Readme